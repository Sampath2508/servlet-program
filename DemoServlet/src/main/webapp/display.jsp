<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Display</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div class="display">
		<p>Name : ${name}</p>
		<p>Age : ${age}</p>
		<p>Email : ${email}</p>
	<div>
</body>
</html>