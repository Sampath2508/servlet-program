package com.DemoServlet;

import java.io.IOException;


import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class DisplayInfo extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		String name = req.getParameter("name").toString();
		Integer age = Integer.parseInt(req.getParameter("age"));
		String email = req.getParameter("email").toString();
		
		req.setAttribute("name", name);
		req.setAttribute("age", age);
		req.setAttribute("email", email);
		
		RequestDispatcher rd = req.getRequestDispatcher("display.jsp");
		rd.forward(req, res);
	}
}
