package com.DemoServlet;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@WebServlet("/postReq")
public class DisplayInfoUsingPost extends HttpServlet{
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String name = req.getParameter("name").toString();
		Integer age = Integer.parseInt(req.getParameter("age"));
		String email = req.getParameter("email").toString();
		
		req.setAttribute("name", name);
		req.setAttribute("age", age);
		req.setAttribute("email", email);
		
		RequestDispatcher rd = req.getRequestDispatcher("display.jsp");
		rd.forward(req, res);
	}
}
